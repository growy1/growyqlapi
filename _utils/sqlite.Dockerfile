FROM alpine
RUN apk update && apk add sqlite && mkdir /db
WORKDIR /db
ENTRYPOINT [ "sqlite3" ]