package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"api/graph/generated"
	"context"

	"github.com/gofrs/uuid"
	"gitlab.com/growy1/seed/schemas"
	"gorm.io/gorm/clause"
)

func (r *userResolver) ID(ctx context.Context, obj *schemas.User) (string, error) {
	return obj.ID.String(), nil
}

func (r *userResolver) Otp(ctx context.Context, obj *schemas.User) (bool, error) {
	return obj.OTPSharedSecret != nil, nil
}

func (r *userResolver) Inventory(ctx context.Context, obj *schemas.User) (*schemas.Inventory, error) {
	if obj.InventoryID == nil {
		return nil, nil
	}
	var err error
	var cvrtUUID uuid.UUID
	if cvrtUUID, err = uuid.FromString(*obj.InventoryID); err != nil {
		return nil, err
	}
	var toget = schemas.Inventory{
		Base: schemas.Base{
			ID: cvrtUUID,
		},
	}
	if err = r.DB.Preload(clause.Associations).First(&toget).Error; err != nil {
		return nil, err
	}
	return &toget, nil
}

// User returns generated.UserResolver implementation.
func (r *Resolver) User() generated.UserResolver { return &userResolver{r} }

type userResolver struct{ *Resolver }
