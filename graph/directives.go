package graph

import (
	"api/graph/auth"
	"context"
	"errors"

	"github.com/99designs/gqlgen/graphql"
	"github.com/gin-gonic/gin"
	"gitlab.com/growy1/seed/schemas"
)

// HasRole check if user has required role
func HasRole(ctx context.Context, obj interface{}, next graphql.Resolver, role schemas.Role) (interface{}, error) {
	var err error
	var gc *gin.Context
	if gc, err = auth.GinContextFromContext(ctx); err != nil {
		return nil, err
	}
	userasking := auth.ForContext(gc)
	if userasking == nil || (userasking.Role != role && userasking.Role != schemas.God) {
		// block calling the next resolver
		return nil, errors.New("Access denied")
	}
	// or let it pass through
	return next(ctx)
}
