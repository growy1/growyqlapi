package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"api/graph/generated"
	"context"

	"gitlab.com/growy1/seed/schemas"
)

func (r *measureResolver) ID(ctx context.Context, obj *schemas.Measure) (string, error) {
	return obj.ID.String(), nil
}

// Measure returns generated.MeasureResolver implementation.
func (r *Resolver) Measure() generated.MeasureResolver { return &measureResolver{r} }

type measureResolver struct{ *Resolver }
