package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"api/graph/generated"
	"context"

	"gitlab.com/growy1/seed/schemas"
)

func (r *seedBankResolver) ID(ctx context.Context, obj *schemas.SeedBank) (int, error) {
	return int(obj.ID), nil
}

// SeedBank returns generated.SeedBankResolver implementation.
func (r *Resolver) SeedBank() generated.SeedBankResolver { return &seedBankResolver{r} }

type seedBankResolver struct{ *Resolver }
