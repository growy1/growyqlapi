package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"api/graph/generated"
	"api/graph/model"
	"api/graph/secrets"
	"bytes"
	"context"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"html"
	"log"
	"net/http"
	"os"

	"github.com/gofrs/uuid"
	"gitlab.com/growy1/goseedfinder"
	"gitlab.com/growy1/seed/schemas"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func (r *mutationResolver) Addmeasure(ctx context.Context, growingSessionID string, input model.MeasureInput) (*schemas.Measure, error) {
	var err error
	sessionID, err := uuid.FromString(growingSessionID)
	if err != nil {
		return nil, err
	}
	nMeasure := schemas.Measure{
		Type:  input.Type,
		Value: input.Value,
		Unit:  input.Unit,
		Date:  input.Date,
	}
	updatedGrowingSession := schemas.GrowingSession{
		Base: schemas.Base{
			ID: sessionID,
		},
		Measures: []schemas.Measure{
			nMeasure,
		},
	}
	if err = r.DB.Updates(&updatedGrowingSession).Error; err != nil {
		return nil, err
	}
	return &nMeasure, err
}

func (r *mutationResolver) Addseedstock(ctx context.Context, inventoryID string, input model.SeedStockInput) (*schemas.SeedStock, error) {
	var err error
	inventoryUUID, err := uuid.FromString(inventoryID)
	if err != nil {
		return nil, err
	}
	nseedStock := schemas.SeedStock{
		StrainID: uint(input.StrainID),
		Count:    input.Count,
		Unit:     &input.Unit,
	}
	updatedInventory := schemas.Inventory{
		Base: schemas.Base{
			ID: inventoryUUID,
		},
		SeedStock: []schemas.SeedStock{
			nseedStock,
		},
	}
	if err = r.DB.Updates(&updatedInventory).Error; err != nil {
		return nil, err
	}
	return &nseedStock, err
}

func (r *mutationResolver) Updateseedstock(ctx context.Context, input model.SeedStockUpdateInput) (*schemas.SeedStock, error) {
	var err error
	var cvrtUUID uuid.UUID
	if cvrtUUID, err = uuid.FromString(input.ID); err != nil {
		return nil, err
	}
	var updateSeedStock = schemas.SeedStock{
		Base: schemas.Base{
			ID: cvrtUUID,
		},
	}
	switch {
	case input.Count != nil:
		updateSeedStock.Count = *input.Count
	case input.StrainID != nil:
		updateSeedStock.StrainID = uint(*input.StrainID)
	case input.Unit != nil:
		updateSeedStock.Unit = input.Unit
	case input.Count == nil && input.StrainID == nil &&
		input.Unit == nil:
		return nil, errors.New("should have at least one field to update")
	}
	if err = r.DB.Updates(&updateSeedStock).Error; err != nil {
		return nil, err
	}
	return &updateSeedStock, nil
}

func (r *mutationResolver) Adddevicestock(ctx context.Context, inventoryID string, input model.DeviceStockInput) (*schemas.DeviceStock, error) {
	var err error
	inventoryUUID, err := uuid.FromString(inventoryID)
	if err != nil {
		return nil, err
	}
	var nDeviceStock = schemas.DeviceStock{
		Count:    input.Count,
		DeviceID: uint(input.DeviceID),
		Unit:     &input.Unit,
	}
	var updatedInventory = schemas.Inventory{
		Base: schemas.Base{
			ID: inventoryUUID,
		},
		DeviceStock: []schemas.DeviceStock{
			nDeviceStock,
		},
	}
	if err = r.DB.Updates(&updatedInventory).Error; err != nil {
		return nil, err
	}
	return &nDeviceStock, err
}

func (r *mutationResolver) Addharveststock(ctx context.Context, inventoryID string, input model.HarvestStockInput) (*schemas.HarvestStock, error) {
	var err error
	inventoryUUID, err := uuid.FromString(inventoryID)
	if err != nil {
		return nil, err
	}
	var nHarvestStock = schemas.HarvestStock{
		Count:   input.Count,
		PlantID: input.PlantID,
		Unit:    &input.Unit,
	}
	var updatedInventory = schemas.Inventory{
		Base: schemas.Base{
			ID: inventoryUUID,
		},
		HarvestStock: []schemas.HarvestStock{
			nHarvestStock,
		},
	}
	if err = r.DB.Updates(&updatedInventory).Error; err != nil {
		return nil, err
	}
	return &nHarvestStock, err
}

func (r *mutationResolver) Updateharveststock(ctx context.Context, id string, count *float64, unit *string) (*schemas.HarvestStock, error) {
	var err error
	harvestStockID, err := uuid.FromString(id)
	if err != nil {
		return nil, err
	}
	var updated = schemas.HarvestStock{
		Base: schemas.Base{
			ID: harvestStockID,
		},
	}
	switch {
	case count != nil:
		updated.Count = *count
	case unit != nil:
		updated.Unit = unit
	case count == nil && unit == nil:
		return nil, errors.New("should have at least one field to update")
	}
	if err = r.DB.Updates(&updated).Error; err != nil {
		return nil, err
	}
	return &updated, err
}

func (r *mutationResolver) Addalarm(ctx context.Context, growingSessionID string, input model.AlarmInput) (*schemas.MeasureAlarm, error) {
	var err error
	sessionID, err := uuid.FromString(growingSessionID)
	if err != nil {
		return nil, err
	}
	if input.Low != nil && input.Low.Type != input.On {
		return nil, errors.New("interval values must be off the same type of alarm 'on'")
	} else if input.High != nil && input.High.Type != input.On {
		return nil, errors.New("interval values must be off the same type of alarm 'on'")
	} else if input.High == nil && input.Low == nil {
		return nil, errors.New("should have at least a high or low value to create alarm")
	}
	var nAlarm = schemas.MeasureAlarm{
		On:               input.On,
		GrowingSessionID: sessionID,
	}
	if input.High != nil {
		var nHighMeasure = schemas.Measure{
			Value: input.High.Value,
			Unit:  input.High.Unit,
			Type:  input.High.Type,
		}
		if err = r.DB.Create(&nHighMeasure).Error; err != nil {
			return nil, err
		}
		nmID := nHighMeasure.ID.String()
		nAlarm.HighID = &nmID
	}
	if input.Low != nil {
		var nlowMeasure = schemas.Measure{
			Value: input.Low.Value,
			Unit:  input.Low.Unit,
			Type:  input.Low.Type,
		}
		if err = r.DB.Create(&nlowMeasure).Error; err != nil {
			return nil, err
		}
		nmID := nlowMeasure.ID.String()
		nAlarm.LowID = &nmID
	}
	var updatedSession = schemas.GrowingSession{
		Base: schemas.Base{
			ID: sessionID,
		},
		Alarms: []schemas.MeasureAlarm{
			nAlarm,
		},
	}
	if err = r.DB.Updates(&updatedSession).Error; err != nil {
		return nil, err
	}
	return &nAlarm, nil
}

func (r *mutationResolver) Deletealarm(ctx context.Context, id string) (*schemas.MeasureAlarm, error) {
	var err error
	var reqUUID uuid.UUID
	if reqUUID, err = uuid.FromString(id); err != nil {
		return nil, err
	}
	var todel = schemas.MeasureAlarm{
		Base: schemas.Base{
			ID: reqUUID,
		},
	}
	if err = r.DB.First(&todel).Error; err != nil {
		return nil, err
	}
	if todel.HighID != nil {
		cvrtUUID, _ := uuid.FromString(*todel.HighID)
		delmeasure := schemas.Measure{
			Base: schemas.Base{
				ID: cvrtUUID,
			},
		}
		if err = r.DB.Delete(&delmeasure).Error; err != nil {
			return nil, err
		}
	}
	if todel.LowID != nil {
		cvrtUUID, _ := uuid.FromString(*todel.LowID)
		delmeasure := schemas.Measure{
			Base: schemas.Base{
				ID: cvrtUUID,
			},
		}
		if err = r.DB.Delete(&delmeasure).Error; err != nil {
			return nil, err
		}
	}
	if err = r.DB.Delete(&todel).Error; err != nil {
		return nil, err
	}
	return &todel, nil
}

func (r *mutationResolver) Adddevice(ctx context.Context, input model.DeviceInput) (*schemas.Device, error) {
	var err error
	var nDevice = schemas.Device{
		Name: input.Name,
		Type: input.Type,
	}
	if err = r.DB.Create(&nDevice).Error; err != nil {
		return nil, err
	}
	return &nDevice, err
}

func (r *mutationResolver) Addpicture(ctx context.Context, growingSessionID string, plantID *string) (*schemas.Picture, error) {
	var err error
	var enc []byte
	client := &http.Client{}
	paparazziURL := os.Getenv("PAPARAZZI_URL")
	sessionID, err := uuid.FromString(growingSessionID)
	if err != nil {
		return nil, err
	}
	photoReq := PhotoRequest{
		GrowingSessionID: sessionID,
	}
	if plantID != nil {
		plantUUID, err := uuid.FromString(*plantID)
		if err != nil {
			return nil, err
		}
		photoReq.PlantID = &plantUUID
		if err = r.DB.First(&schemas.Plant{
			Base: schemas.Base{
				ID: sessionID,
			},
		}).Error; err != nil {
			return nil, err
		}
	}
	if err = r.DB.First(&schemas.GrowingSession{
		Base: schemas.Base{
			ID: sessionID,
		},
	}).Error; err != nil {
		return nil, err
	}
	if paparazziURL == "" {
		paparazziURL = "http://localhost:8081/api/v1"
	}
	if enc, err = json.Marshal(photoReq); err != nil {
		return nil, err
	}
	req, err := http.NewRequest("PUT", paparazziURL+"/capture", bytes.NewBuffer(enc))
	if APIUser != "" && APIKey != "" {
		req.SetBasicAuth(APIUser, APIKey)
	}
	response, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()
	var respfmt schemas.Picture
	jsondec := json.NewDecoder(response.Body)
	jsondec.DisallowUnknownFields()
	if err = jsondec.Decode(&respfmt); err != nil {
		return nil, err
	}
	updated := schemas.GrowingSession{
		Base: schemas.Base{
			ID: sessionID,
		},
		Pictures: []schemas.Picture{
			respfmt,
		},
	}
	if err = r.DB.Updates(&updated).Error; err != nil {
		return nil, err
	}
	return &respfmt, err
}

func (r *mutationResolver) Updatepicture(ctx context.Context, input model.UpdatePictureInput) (*schemas.Picture, error) {
	var err error
	var cvrtUUID uuid.UUID
	if cvrtUUID, err = uuid.FromString(input.ID); err != nil {
		return nil, err
	}
	var updatePicture = schemas.Picture{
		Base: schemas.Base{
			ID: cvrtUUID,
		},
	}
	switch {
	case input.FilePath != nil:
		updatePicture.FilePath = *input.FilePath
	case input.TakenAt != nil:
		updatePicture.TakenAt = *input.TakenAt
	case input.TakenAt == nil && input.FilePath == nil:
		return nil, errors.New("should have at least one field to update")
	}
	if err = r.DB.Updates(&updatePicture).Error; err != nil {
		return nil, err
	}
	return &updatePicture, nil
}

func (r *mutationResolver) Deletepicture(ctx context.Context, id string) (*schemas.Picture, error) {
	var err error
	var enc []byte
	var reqUUID uuid.UUID
	client := &http.Client{}
	paparazziURL := os.Getenv("PAPARAZZI_URL")

	if reqUUID, err = uuid.FromString(id); err != nil {
		return nil, err
	}
	var todel = schemas.Picture{
		Base: schemas.Base{
			ID: reqUUID,
		},
	}
	if err = r.DB.Find(&todel).Error; err != nil {
		return nil, err
	}
	if err != nil {
		return nil, err
	}
	dropReq := DropRequest{
		FilePath: todel.FilePath,
	}
	if paparazziURL == "" {
		paparazziURL = "http://localhost:8081/api/v1"
	}
	if enc, err = json.Marshal(dropReq); err != nil {
		return nil, err
	}
	req, err := http.NewRequest("DELETE", paparazziURL+"/drop", bytes.NewBuffer(enc))
	if APIUser != "" && APIKey != "" {
		req.SetBasicAuth(APIUser, APIKey)
	}
	response, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()
	if response.StatusCode != http.StatusOK {
		return nil, errors.New("backend error, failed to delete picture")
	}
	if err = r.DB.Exec("DELETE FROM growingsession_pictures WHERE picture_id = ?", id).Error; err != nil {
		log.Printf("[ERROR] Failed to delete related growing session picture : %s\n", err.Error())
	}
	if err = r.DB.Exec("DELETE FROM plant_pictures WHERE picture_id = ?", id).Error; err != nil {
		log.Printf("[ERROR] Failed to delete related plant picture : %s\n", err.Error())
	}
	if err = r.DB.Delete(&todel).Error; err != nil {
		return nil, err
	}
	return &todel, err
}

func (r *mutationResolver) Updateplant(ctx context.Context, input model.UpdatePlantInput) (*schemas.Plant, error) {
	var err error
	var cvrtUUID uuid.UUID
	if cvrtUUID, err = uuid.FromString(input.ID); err != nil {
		return nil, err
	}
	var updatePlant = schemas.Plant{
		Base: schemas.Base{
			ID: cvrtUUID,
		},
	}
	switch {
	case input.Name != nil:
		updatePlant.Name = *input.Name
	case input.StartedAt != nil:
		updatePlant.StartedAt = *input.StartedAt
	case input.EndedAt != nil:
		updatePlant.EndedAt = input.EndedAt
	case input.StrainID != nil:
		val := uint(*input.StrainID)
		updatePlant.StrainID = val
	case input.Name == nil && input.StartedAt == nil &&
		input.EndedAt == nil && input.StrainID == nil:
		return nil, errors.New("should have at least one field to update")
	}
	if err = r.DB.Updates(&updatePlant).Error; err != nil {
		return nil, err
	}
	return &updatePlant, nil
}

func (r *mutationResolver) Deleteplant(ctx context.Context, id string) (*schemas.Plant, error) {
	var err error
	var reqUUID uuid.UUID
	if reqUUID, err = uuid.FromString(id); err != nil {
		return nil, err
	}
	var todel = schemas.Plant{
		Base: schemas.Base{
			ID: reqUUID,
		},
	}
	if err = r.DB.Preload(clause.Associations).Find(&todel).Error; err != nil {
		return nil, err
	}
	switch {
	case len(todel.Pictures) > 0:
		if err = r.DB.Delete(&todel.Pictures).Error; err != nil {
			return nil, err
		}
	case len(todel.States) > 0:
		if err = r.DB.Delete(&todel.States).Error; err != nil {
			return nil, err
		}
	}
	if err = r.DB.Delete(&todel).Error; err != nil {
		return nil, err
	}
	return &todel, err
}

func (r *mutationResolver) Addgrowstate(ctx context.Context, plantID string, input model.GrowStateInput) (*schemas.GrowState, error) {
	var err error
	var reqUUID uuid.UUID
	if reqUUID, err = uuid.FromString(plantID); err != nil {
		return nil, err
	}
	ngrowState := schemas.GrowState{
		DevelopmentState: input.DevelopmentState,
		StartedAt:        input.StartedAt,
		EndedAt:          input.EndedAt,
		Comments:         input.Comments,
	}
	plantUpdate := schemas.Plant{
		Base: schemas.Base{
			ID: reqUUID,
		},
		States: []schemas.GrowState{
			ngrowState,
		},
	}
	if err = r.DB.Updates(&plantUpdate).Error; err != nil {
		return nil, err
	}
	return &ngrowState, nil
}

func (r *mutationResolver) Updategrowstate(ctx context.Context, input model.UpdateGrowStateInput) (*schemas.GrowState, error) {
	var err error
	var cvrtUUID uuid.UUID
	if cvrtUUID, err = uuid.FromString(input.ID); err != nil {
		return nil, err
	}
	var updateGrowState = schemas.GrowState{
		Base: schemas.Base{
			ID: cvrtUUID,
		},
	}
	switch {
	case input.DevelopmentState != nil:
		updateGrowState.DevelopmentState = *input.DevelopmentState
	case input.StartedAt != nil:
		updateGrowState.StartedAt = *input.StartedAt
	case input.EndedAt != nil:
		updateGrowState.EndedAt = input.EndedAt
	case input.Comments != nil:
		updateGrowState.Comments = input.Comments
	case input.DevelopmentState == nil && input.StartedAt == nil &&
		input.EndedAt == nil && input.Comments == nil:
		return nil, errors.New("should have at least one field to update")
	}
	if err = r.DB.Updates(&updateGrowState).Error; err != nil {
		return nil, err
	}
	return &updateGrowState, nil
}

func (r *mutationResolver) Deletegrowstate(ctx context.Context, id string) (*schemas.GrowState, error) {
	var err error
	var reqUUID uuid.UUID
	if reqUUID, err = uuid.FromString(id); err != nil {
		return nil, err
	}
	// TODO: add DEL for paparazzi route too
	var todel = schemas.GrowState{
		Base: schemas.Base{
			ID: reqUUID,
		},
	}
	if err = r.DB.Delete(&todel).Error; err != nil {
		return nil, err
	}
	return &todel, err
}

func (r *mutationResolver) Addseeedbank(ctx context.Context, input model.SeedBankInput) (*schemas.SeedBank, error) {
	var err error
	var nSeedBank = schemas.SeedBank{
		Name:    input.Name,
		LogoURL: input.LogoURL,
	}
	if err = r.DB.Create(&nSeedBank).Error; err != nil {
		return nil, err
	}
	return &nSeedBank, err
}

func (r *mutationResolver) Addstrain(ctx context.Context, input model.StrainInput) (*schemas.Strain, error) {
	var err error
	nStrain := schemas.Strain{
		Supplier:    input.Supplier,
		Link:        input.Link,
		Name:        input.Name,
		Genetic:     input.Genetic,
		Photoperiod: input.Photoperiod,
	}
	for el := range input.Type {
		nStrain.Type = append(nStrain.Type, schemas.StrainTypeInfos{
			Type:       input.Type[el].Type,
			Percentage: input.Type[el].Percentage,
		})
	}
	for el := range input.Cannabinoids {
		nStrain.Cannabinoids = append(nStrain.Cannabinoids, schemas.Cannabinoid{
			Name:       input.Cannabinoids[el].Name,
			Percentage: input.Cannabinoids[el].Percentage,
		})
	}
	for el := range input.FloweringTimes {
		nStrain.FloweringTimes = append(nStrain.FloweringTimes, schemas.Measure{
			Type:  input.FloweringTimes[el].Type,
			Value: input.FloweringTimes[el].Value,
			Unit:  input.FloweringTimes[el].Unit,
		})
	}
	if input.SeedBankID != nil {
		nStrain.SeedBankID = uint(*input.SeedBankID)
	}
	if err = r.DB.Create(&nStrain).Error; err != nil {
		return nil, err
	}
	return &nStrain, nil
}

func (r *mutationResolver) Updatestrain(ctx context.Context, input model.UpdateStrainInput) (*schemas.Strain, error) {
	var err error
	var updateStrain = schemas.Strain{
		Model: gorm.Model{
			ID: uint(input.ID),
		},
	}
	switch {
	case input.Link != nil:
		updateStrain.Link = *input.Link
	case input.SeedBankID != nil:
		val := uint(*input.SeedBankID)
		updateStrain.SeedBankID = val
	case input.Name != nil:
		updateStrain.Name = *input.Name
	case input.Genetic != nil:
		updateStrain.Genetic = *input.Genetic
	case input.Photoperiod != nil:
		updateStrain.Photoperiod = *input.Photoperiod
	case input.ImageURL != nil:
		updateStrain.ImageURL = input.ImageURL
	case input.Link == nil && input.SeedBankID == nil &&
		input.Name == nil && input.Genetic == nil &&
		input.Photoperiod == nil && input.ImageURL == nil:
		return nil, errors.New("should have at least one field to update")
	}
	if err = r.DB.Updates(&updateStrain).Error; err != nil {
		return nil, err
	}
	return &updateStrain, nil
}

func (r *mutationResolver) Deletestrain(ctx context.Context, id int) (*schemas.Strain, error) {
	var err error
	var todel = schemas.Strain{
		Model: gorm.Model{
			ID: uint(id),
		},
	}
	if err = r.DB.Delete(&todel).Error; err != nil {
		return nil, err
	}
	return &todel, err
}

func (r *mutationResolver) Addstrainseedfinderinfo(ctx context.Context, strainID int, input model.SeedFinderInput) (*schemas.SeedFinderStrainInfo, error) {
	var err error
	var apiclient goseedfinder.Client
	var apiresult *goseedfinder.StrainInfo
	var updateStrain = schemas.Strain{
		Model: gorm.Model{
			ID: uint(strainID),
		},
	}
	var nseedfinderInfo = schemas.SeedFinderStrainInfo{
		SeedFinderID:        input.SeedFinderID,
		SeedFinderBreederID: input.SeedFinderBreederID,
	}
	var seedfinderAPIKey = secrets.MustGetSecret("SEEDFINDER_APIKEY")
	apiclient = goseedfinder.New(seedfinderAPIKey)
	if apiresult, err = apiclient.StrainInfo(input.SeedFinderID, input.SeedFinderBreederID); err != nil {
		fmt.Printf("[ERROR] Addstrainseedfinderinfo : %s\n", err.Error())
		return nil, errors.New("seedfinder api error")
	}
	unescaped := html.UnescapeString(apiresult.BreederInfo.Description)
	nseedfinderInfo.SFStrainHTMLDescription = &unescaped
	if err = r.DB.Create(&nseedfinderInfo).Error; err != nil {
		return nil, err
	}
	updateStrain.SeedFinderStrainInfoID = &nseedfinderInfo.ID
	if err = r.DB.Updates(&updateStrain).Error; err != nil {
		return nil, err
	}
	return &nseedfinderInfo, err
}

func (r *mutationResolver) AddstrainfloweringTimes(ctx context.Context, strainID int, input []model.MeasureInput) ([]schemas.Measure, error) {
	var err error
	var nfloweringTimes []schemas.Measure
	var updateStrain = schemas.Strain{
		Model: gorm.Model{
			ID: uint(strainID),
		},
	}
	for el := range input {
		nfloweringTime := schemas.Measure{
			Type:  input[el].Type,
			Value: input[el].Value,
			Unit:  input[el].Unit,
		}
		nfloweringTimes = append(nfloweringTimes, nfloweringTime)
	}
	updateStrain.FloweringTimes = nfloweringTimes
	if err = r.DB.Updates(&updateStrain).Error; err != nil {
		return nil, err
	}
	return nfloweringTimes, err
}

func (r *mutationResolver) DeletestrainfloweringTimes(ctx context.Context, strainID int, measureID string) (*schemas.Measure, error) {
	var err error
	var reqUUID uuid.UUID
	if reqUUID, err = uuid.FromString(measureID); err != nil {
		return nil, err
	}
	if err = r.DB.
		Exec("DELETE FROM strain_flowering_times WHERE measure_id = @measureId AND strain_id = @strainId",
			sql.Named("measureId", measureID),
			sql.Named("strainId", strainID),
		).Error; err != nil {
		return nil, err
	}
	toDel := schemas.Measure{
		Base: schemas.Base{
			ID: reqUUID,
		},
	}
	if err = r.DB.Delete(&toDel).Error; err != nil {
		return nil, err
	}
	return &toDel, err
}

func (r *mutationResolver) Addstraintypeinfos(ctx context.Context, strainID int, input []model.StrainTypeInput) ([]schemas.StrainTypeInfos, error) {
	var err error
	var nstrainTypeInfos []schemas.StrainTypeInfos
	var updateStrain = schemas.Strain{
		Model: gorm.Model{
			ID: uint(strainID),
		},
	}
	for el := range input {
		nstrainTypeInfo := schemas.StrainTypeInfos{
			StrainID:   uint(strainID),
			Type:       input[el].Type,
			Percentage: input[el].Percentage,
		}
		nstrainTypeInfos = append(nstrainTypeInfos, nstrainTypeInfo)
	}
	updateStrain.Type = nstrainTypeInfos
	if err = r.DB.Updates(&updateStrain).Error; err != nil {
		return nil, err
	}
	return nstrainTypeInfos, err
}

func (r *mutationResolver) Deletestraintypeinfos(ctx context.Context, strainID int, straintypeinfosID int) (*schemas.StrainTypeInfos, error) {
	var err error
	if err = r.DB.
		Exec("DELETE FROM strain_straintypeinfos WHERE strain_type_infos_id = @straintypeinfosId AND strain_id = @strainId",
			sql.Named("straintypeinfosId", straintypeinfosID),
			sql.Named("strainId", strainID),
		).Error; err != nil {
		return nil, err
	}
	toDel := schemas.StrainTypeInfos{
		Model: gorm.Model{
			ID: uint(straintypeinfosID),
		},
	}
	if err = r.DB.Delete(&toDel).Error; err != nil {
		return nil, err
	}
	return &toDel, err
}

func (r *mutationResolver) Addstraincannabinoids(ctx context.Context, strainID int, input []model.CannabinoidInput) ([]schemas.Cannabinoid, error) {
	var err error
	var nstrainCannabinoids []schemas.Cannabinoid
	var updateStrain = schemas.Strain{
		Model: gorm.Model{
			ID: uint(strainID),
		},
	}
	for el := range input {
		nstrainCannabinoid := schemas.Cannabinoid{
			StrainID:   uint(strainID),
			Name:       input[el].Name,
			Percentage: input[el].Percentage,
		}
		nstrainCannabinoids = append(nstrainCannabinoids, nstrainCannabinoid)
	}
	updateStrain.Cannabinoids = nstrainCannabinoids
	if err = r.DB.Updates(&updateStrain).Error; err != nil {
		return nil, err
	}
	return nstrainCannabinoids, err
}

func (r *mutationResolver) Deletestraincannabinoid(ctx context.Context, strainID int, cannabinoidID int) (*schemas.Cannabinoid, error) {
	var err error
	if err = r.DB.
		Exec("DELETE FROM strain_cannabinoids WHERE cannabinoid_id = @cannabinoidId AND strain_id = @strainId",
			sql.Named("cannabinoidId", cannabinoidID),
			sql.Named("strainId", strainID),
		).Error; err != nil {
		return nil, err
	}
	toDel := schemas.Cannabinoid{
		Model: gorm.Model{
			ID: uint(cannabinoidID),
		},
	}
	if err = r.DB.Delete(&toDel).Error; err != nil {
		return nil, err
	}
	return &toDel, err
}

func (r *mutationResolver) Addgrowingsession(ctx context.Context, input model.GrowingSessionInput) (*schemas.GrowingSession, error) {
	var err error
	var reqUUID uuid.UUID
	if reqUUID, err = uuid.FromString(input.UserID); err != nil {
		return nil, err
	}
	var nplants []schemas.Plant
	var nboxvolumes []schemas.Measure
	var nGrowingSession = schemas.GrowingSession{
		Name:      input.Name,
		Type:      input.Type,
		State:     input.State,
		StartedAt: input.StartedAt,
		EndedAt:   input.EndedAt,
	}
	for el := range input.Plants {
		nPlant := schemas.Plant{
			Name:      input.Plants[el].Name,
			StartedAt: input.Plants[el].StartedAt,
			EndedAt:   input.Plants[el].EndedAt,
		}
		if input.Plants[el].StrainID != nil {
			nPlant.StrainID = uint(*input.Plants[el].StrainID)
		}
		nplants = append(nplants, nPlant)
	}
	nGrowingSession.Plants = nplants
	for el := range input.BoxVolume {
		nMeasure := schemas.Measure{
			Type:  input.BoxVolume[el].Type,
			Value: input.BoxVolume[el].Value,
			Unit:  input.BoxVolume[el].Unit,
		}
		nboxvolumes = append(nboxvolumes, nMeasure)
	}
	nGrowingSession.BoxVolume = nboxvolumes
	if err = r.DB.Create(&schemas.User{
		Base: schemas.Base{
			ID: reqUUID,
		},
		GrowingSessions: []schemas.GrowingSession{
			nGrowingSession,
		},
	}).Error; err != nil {
		return nil, err
	}
	return &nGrowingSession, nil
}

func (r *mutationResolver) Deletegrowingsession(ctx context.Context, id string) (*bool, error) {
	var ok = true
	var err error
	var cvrtUUID uuid.UUID
	if cvrtUUID, err = uuid.FromString(id); err != nil {
		return nil, err
	}
	var toDelete = schemas.GrowingSession{
		Base: schemas.Base{
			ID: cvrtUUID,
		},
	}
	if err = r.DB.Exec("delete from user_growingsessions where growing_session_id = ?", id).Error; err != nil {
		log.Printf("[ERROR] Failed to delete related user growing session : %s\n", err.Error())
	}
	if err = r.DB.Delete(&toDelete).Error; err != nil {
		ok = false
		return &ok, err
	}
	return &ok, err
}

func (r *mutationResolver) Register(ctx context.Context, input model.RegisterInput) (*schemas.User, error) {
	var InvitationCode = secrets.MustGetSecret("INVITATION_CODE")
	if input.InvitationKey != InvitationCode {
		return nil, errors.New("bad invite code")
	}
	var nuser = schemas.User{
		Name:     input.Name,
		Role:     schemas.Grower,
		Password: &input.Password,
		Email:    input.Email,
	}
	if err := r.DB.Create(&nuser).Error; err != nil {
		return nil, err
	}
	nuser.Password = nil
	return &nuser, nil
}

func (r *mutationResolver) Updateuserfcmtoken(ctx context.Context, userID string, fcmToken string) (*schemas.User, error) {
	var err error
	var cvrtUUID uuid.UUID
	if cvrtUUID, err = uuid.FromString(userID); err != nil {
		return nil, err
	}
	var toUpdate = schemas.User{
		Base: schemas.Base{
			ID: cvrtUUID,
		},
		FCMToken: &fcmToken,
	}
	if err = r.DB.Updates(&toUpdate).Error; err != nil {
		return nil, err
	}
	return &toUpdate, err
}

func (r *mutationResolver) Updateuser(ctx context.Context, input model.UpdateUserInput) (*schemas.User, error) {
	var err error
	var cvrtUUID uuid.UUID
	if cvrtUUID, err = uuid.FromString(input.ID); err != nil {
		return nil, err
	}
	var toUpdate = schemas.User{
		Base: schemas.Base{
			ID: cvrtUUID,
		},
	}
	switch {
	case input.Name != nil:
		toUpdate.Name = *input.Name
	case input.Email != nil:
		toUpdate.Email = input.Email
	case input.Password != nil:
		toUpdate.Password = input.Password
	case input.Inventory != nil && *input.Inventory == true:
		nInventory := schemas.Inventory{}
		if err = r.DB.Create(&nInventory).Error; err != nil {
			return nil, err
		}
		ninventoryID := nInventory.ID.String()
		toUpdate.InventoryID = &ninventoryID
	case input.Name == nil && input.Email == nil &&
		input.Password == nil && input.Inventory == nil:
		return nil, errors.New("should have at least one field to update")
	}
	if err = r.DB.Updates(&toUpdate).Error; err != nil {
		return nil, err
	}
	return &toUpdate, err
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

type mutationResolver struct{ *Resolver }
