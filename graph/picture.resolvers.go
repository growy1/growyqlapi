package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"api/graph/generated"
	"context"
	"os"

	"gitlab.com/growy1/seed/schemas"
)

func (r *pictureResolver) ID(ctx context.Context, obj *schemas.Picture) (string, error) {
	return obj.ID.String(), nil
}

func (r *pictureResolver) URL(ctx context.Context, obj *schemas.Picture) (*string, error) {
	res := ""
	if obj == nil {
		return &res, nil
	}
	cdnURL := os.Getenv("CDN_URL")
	if cdnURL == "" {
		cdnURL = "http://localhost:8082"
	}
	res = cdnURL + "/" + obj.FilePath
	return &res, nil
}

// Picture returns generated.PictureResolver implementation.
func (r *Resolver) Picture() generated.PictureResolver { return &pictureResolver{r} }

type pictureResolver struct{ *Resolver }
