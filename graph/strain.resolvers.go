package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"api/graph/generated"
	"context"
	"fmt"

	"gitlab.com/growy1/seed/schemas"
	"gorm.io/gorm"
)

func (r *cannabinoidResolver) ID(ctx context.Context, obj *schemas.Cannabinoid) (int, error) {
	return int(obj.ID), nil
}

func (r *seedFinderStrainInfoResolver) ID(ctx context.Context, obj *schemas.SeedFinderStrainInfo) (int, error) {
	return int(obj.ID), nil
}

func (r *strainResolver) ID(ctx context.Context, obj *schemas.Strain) (int, error) {
	return int(obj.ID), nil
}

func (r *strainResolver) SeedBank(ctx context.Context, obj *schemas.Strain) (*schemas.SeedBank, error) {
	var err error
	var res = schemas.SeedBank{
		Model: gorm.Model{
			ID: obj.SeedBankID,
		},
	}
	if err = r.DB.Find(&res).Error; err != nil {
		return nil, err
	}
	return &res, nil
}

func (r *strainResolver) SeedFinderStrainInfo(ctx context.Context, obj *schemas.Strain) (*schemas.SeedFinderStrainInfo, error) {
	var err error
	if obj.SeedFinderStrainInfoID == nil {
		return nil, nil
	}
	var res = schemas.SeedFinderStrainInfo{
		Model: gorm.Model{
			ID: *obj.SeedFinderStrainInfoID,
		},
	}
	if err = r.DB.Find(&res).Error; err != nil {
		return nil, err
	}
	return &res, nil
}

func (r *strainTypeInfosResolver) ID(ctx context.Context, obj *schemas.StrainTypeInfos) (int, error) {
	return int(obj.ID), nil
}

// Cannabinoid returns generated.CannabinoidResolver implementation.
func (r *Resolver) Cannabinoid() generated.CannabinoidResolver { return &cannabinoidResolver{r} }

// SeedFinderStrainInfo returns generated.SeedFinderStrainInfoResolver implementation.
func (r *Resolver) SeedFinderStrainInfo() generated.SeedFinderStrainInfoResolver {
	return &seedFinderStrainInfoResolver{r}
}

// Strain returns generated.StrainResolver implementation.
func (r *Resolver) Strain() generated.StrainResolver { return &strainResolver{r} }

// StrainTypeInfos returns generated.StrainTypeInfosResolver implementation.
func (r *Resolver) StrainTypeInfos() generated.StrainTypeInfosResolver {
	return &strainTypeInfosResolver{r}
}

type cannabinoidResolver struct{ *Resolver }
type seedFinderStrainInfoResolver struct{ *Resolver }
type strainResolver struct{ *Resolver }
type strainTypeInfosResolver struct{ *Resolver }

// !!! WARNING !!!
// The code below was going to be deleted when updating resolvers. It has been copied here so you have
// one last chance to move it out of harms way if you want. There are two reasons this happens:
//  - When renaming or deleting a resolver the old code will be put in here. You can safely delete
//    it when you're done.
//  - You have helper methods in this file. Move them out to keep these resolver files clean.
func (r *seedFinderStrainInfoResolver) SfStrainInfoHTMLDescription(ctx context.Context, obj *schemas.SeedFinderStrainInfo) (*string, error) {
	panic(fmt.Errorf("not implemented"))
}
