package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"api/graph/auth"
	"api/graph/generated"
	"api/graph/model"
	"api/graph/secrets"
	"context"
	"errors"
	"log"

	"github.com/gofrs/uuid"
	"gitlab.com/growy1/seed/schemas"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func (r *queryResolver) Growingsessions(ctx context.Context, input *model.GrowingSessionSearchInput) ([]schemas.GrowingSession, error) {
	var elems []schemas.GrowingSession
	if err := r.DB.
		Preload("BoxVolume").
		Preload("Plants").
		Preload("Devices").
		Preload("Pictures", func(db *gorm.DB) *gorm.DB {
			return db.Order("pictures.taken_at DESC")
		}).
		Find(&elems).Error; err != nil {
		return nil, err
	}
	return elems, nil
}

func (r *queryResolver) Growingsession(ctx context.Context, id string) (*schemas.GrowingSession, error) {
	var err error
	var cvrtUUID uuid.UUID
	if cvrtUUID, err = uuid.FromString(id); err != nil {
		return nil, err
	}
	var elem = schemas.GrowingSession{
		Base: schemas.Base{
			ID: cvrtUUID,
		},
	}
	if err := r.DB.
		Preload("BoxVolume").
		Preload("Plants").
		Preload("Devices").
		Preload("Alarms").
		Preload("Pictures", func(db *gorm.DB) *gorm.DB {
			return db.Order("pictures.taken_at DESC")
		}).
		First(&elem).Error; err != nil {
		return nil, err
	}
	return &elem, nil
}

func (r *queryResolver) Inventory(ctx context.Context, id string) (*schemas.Inventory, error) {
	var err error
	var cvrtUUID uuid.UUID
	if cvrtUUID, err = uuid.FromString(id); err != nil {
		return nil, err
	}
	var elem = schemas.Inventory{
		Base: schemas.Base{
			ID: cvrtUUID,
		},
	}
	if err := r.DB.Preload(clause.Associations).First(&elem).Error; err != nil {
		return nil, err
	}
	return &elem, err
}

func (r *queryResolver) Alarms(ctx context.Context, growingSessionID string) ([]schemas.MeasureAlarm, error) {
	var err error
	var elems []schemas.MeasureAlarm
	if err = r.DB.
		Where("growing_session_id = ?", growingSessionID).
		Find(&elems).Error; err != nil {
		return nil, err
	}
	return elems, nil
}

func (r *queryResolver) Devices(ctx context.Context, growingSessionID *string) ([]schemas.Device, error) {
	var err error
	var elems []schemas.Device
	if growingSessionID != nil {
		if err = r.DB.
			Unscoped().
			Joins("JOIN growingsession_devices ON device_id = devices.id WHERE growing_session_id = ?", *growingSessionID).
			Find(&elems).Error; err != nil {
			return nil, err
		}
	} else {
		if err = r.DB.Preload(clause.Associations).Find(&elems).Error; err != nil {
			return nil, err
		}
	}
	return elems, nil
}

func (r *queryResolver) Pictures(ctx context.Context, growingSessionID string) ([]schemas.Picture, error) {
	var err error
	var elems []schemas.Picture
	if err = r.DB.
		Joins("JOIN growingsession_pictures ON picture_id = pictures.id WHERE growing_session_id = ?", growingSessionID).
		Order("taken_at DESC").
		Find(&elems).Error; err != nil {
		return nil, err
	}
	return elems, nil
}

func (r *queryResolver) Measures(ctx context.Context, growingSessionID string, typeArg *schemas.DataType, pagination *model.PaginationInput) ([]schemas.Measure, error) {
	var err error
	var elems []schemas.Measure
	var defCounts = 6 * 24 // We have 6 measures per hour so this is one day measures
	var defPagination = model.PaginationInput{
		Count:  defCounts,
		Offset: 0,
	}
	if pagination != nil {
		defPagination.Count = pagination.Count
		defPagination.Offset = pagination.Offset
	}
	if typeArg != nil {
		if err = r.DB.
			Joins("JOIN growingsession_measures ON measure_id = measures.id").
			Where("growing_session_id = ? AND measures.type = ?", growingSessionID, typeArg).
			Order("date DESC").
			Limit(defPagination.Count).
			Offset(defPagination.Offset).
			Find(&elems).Error; err != nil {
			return nil, err
		}
	} else if err = r.DB.
		Joins("JOIN growingsession_measures ON measure_id = measures.id WHERE growing_session_id = ?", growingSessionID).
		Order("date DESC").
		Limit(defPagination.Count).
		Offset(defPagination.Offset).
		Find(&elems).Error; err != nil {
		return nil, err
	}
	return elems, nil
}

func (r *queryResolver) Measure(ctx context.Context, growingSessionID string, measureID *string, typeArg schemas.DataType) (*schemas.Measure, error) {
	var err error
	var meascvrtUUID uuid.UUID
	var elem schemas.Measure

	if measureID != nil {
		if meascvrtUUID, err = uuid.FromString(*measureID); err != nil {
			return nil, err
		}
		elem.Base.ID = meascvrtUUID
	}
	if err = r.DB.
		Joins("JOIN growingsession_measures ON measure_id = measures.id").
		Where("growing_session_id = ? AND measures.type = ?", growingSessionID, typeArg).
		Order("date DESC").
		First(&elem).
		Error; err != nil {
		return nil, err
	}
	return &elem, err
}

func (r *queryResolver) Strains(ctx context.Context, seedBankID *string) ([]schemas.Strain, error) {
	var elems []schemas.Strain
	if seedBankID != nil {
		if err := r.DB.
			Preload("Type", func(db *gorm.DB) *gorm.DB {
				return db.Order("strain_type_infos.percentage DESC")
			}).
			Preload("Cannabinoids", func(db *gorm.DB) *gorm.DB {
				return db.Order("cannabinoids.percentage DESC")
			}).
			Preload("FloweringTimes", func(db *gorm.DB) *gorm.DB {
				return db.Order("measures.value ASC")
			}).
			Where("seed_bank_id = ?", *seedBankID).
			Find(&elems).Error; err != nil {
			return nil, err
		}
	} else {
		if err := r.DB.
			Preload("Type", func(db *gorm.DB) *gorm.DB {
				return db.Order("strain_type_infos.percentage DESC")
			}).
			Preload("Cannabinoids", func(db *gorm.DB) *gorm.DB {
				return db.Order("cannabinoids.percentage DESC")
			}).
			Preload("FloweringTimes", func(db *gorm.DB) *gorm.DB {
				return db.Order("measures.value ASC")
			}).
			Find(&elems).Error; err != nil {
			return nil, err
		}
	}
	return elems, nil
}

func (r *queryResolver) Strain(ctx context.Context, id int) (*schemas.Strain, error) {
	var elem = schemas.Strain{
		Model: gorm.Model{
			ID: uint(id),
		},
	}
	if err := r.DB.
		Preload("Type", func(db *gorm.DB) *gorm.DB {
			return db.Order("strain_type_infos.percentage DESC")
		}).
		Preload("Cannabinoids", func(db *gorm.DB) *gorm.DB {
			return db.Order("cannabinoids.percentage DESC")
		}).
		Preload("FloweringTimes", func(db *gorm.DB) *gorm.DB {
			return db.Order("measures.value ASC")
		}).
		First(&elem).Error; err != nil {
		return nil, err
	}
	return &elem, nil
}

func (r *queryResolver) Seedbanks(ctx context.Context) ([]schemas.SeedBank, error) {
	var elems []schemas.SeedBank
	if err := r.DB.Find(&elems).Error; err != nil {
		return nil, err
	}
	return elems, nil
}

func (r *queryResolver) Seedbank(ctx context.Context, id int) (*schemas.SeedBank, error) {
	var elem = schemas.SeedBank{
		Model: gorm.Model{
			ID: uint(id),
		},
	}
	if err := r.DB.First(&elem).Error; err != nil {
		return nil, err
	}
	return &elem, nil
}

func (r *queryResolver) Plants(ctx context.Context, growingSessionID string, input *model.PlantSearchInput) ([]schemas.Plant, error) {
	var err error
	var elems []schemas.Plant
	if err = r.DB.
		Joins("JOIN growingsession_plants ON plant_id = plants.id WHERE growing_session_id = ?", growingSessionID).
		Preload(clause.Associations).
		Find(&elems).Error; err != nil {
		return nil, err
	}
	return elems, nil
}

func (r *queryResolver) Plant(ctx context.Context, id string) (*schemas.Plant, error) {
	var err error
	var cvrtUUID uuid.UUID
	if cvrtUUID, err = uuid.FromString(id); err != nil {
		return nil, err
	}
	var elem = schemas.Plant{
		Base: schemas.Base{
			ID: cvrtUUID,
		},
	}
	if err := r.DB.Preload(clause.Associations).First(&elem).Error; err != nil {
		return nil, err
	}
	return &elem, nil
}

func (r *queryResolver) Users(ctx context.Context) ([]schemas.User, error) {
	var err error
	var elems []schemas.User
	if err = r.DB.Preload(clause.Associations).Find(&elems).Error; err != nil {
		return nil, err
	}
	return elems, err
}

func (r *queryResolver) User(ctx context.Context, id string) (*schemas.User, error) {
	var err error
	var cvrtUUID uuid.UUID
	if cvrtUUID, err = uuid.FromString(id); err != nil {
		return nil, err
	}
	gc, err := auth.GinContextFromContext(ctx)
	if err != nil {
		return nil, err
	}
	userasking := auth.ForContext(gc)
	if userasking != nil {
		log.Printf("[INFO] user id %s is gathering info on user %s\n", userasking.ID, id)
	}

	var elem = schemas.User{
		Base: schemas.Base{
			ID: cvrtUUID,
		},
	}
	if err := r.DB.
		Preload(clause.Associations).
		First(&elem).Error; err != nil {
		return nil, err
	}
	return &elem, nil
}

func (r *queryResolver) Login(ctx context.Context, name string, password *string, otp *int) (*model.Session, error) {
	if password == nil && otp == nil {
		return nil, errors.New("invalid request")
	}
	var err error
	var token string
	var user = schemas.User{
		Name: name,
	}
	if err = r.DB.
		Preload(clause.Associations).
		Where("name = ?", user.Name).
		First(&user).Error; err != nil {
		return nil, errors.New("unknow user")
	}
	if err = bcrypt.CompareHashAndPassword([]byte(user.PasswordHash), []byte(*password)); err != nil {
		return nil, err
	}
	if token, err = secrets.GetTokenString(user.Base.ID.String()); err != nil {
		return nil, err
	}
	return &model.Session{
		Token: token,
		User:  &user,
	}, nil
}

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type queryResolver struct{ *Resolver }
