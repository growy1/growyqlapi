package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"api/graph/generated"
	"context"

	"gitlab.com/growy1/seed/schemas"
)

func (r *deviceResolver) ID(ctx context.Context, obj *schemas.Device) (int, error) {
	return int(obj.ID), nil
}

func (r *deviceResolver) PowerID(ctx context.Context, obj *schemas.Device) (*int, error) {
	res := obj.PowerID
	if res != nil {
		val := int(*res)
		return &val, nil
	}
	return nil, nil
}

// Device returns generated.DeviceResolver implementation.
func (r *Resolver) Device() generated.DeviceResolver { return &deviceResolver{r} }

type deviceResolver struct{ *Resolver }
