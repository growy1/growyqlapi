package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"api/graph/generated"
	"context"

	"github.com/gofrs/uuid"
	"gitlab.com/growy1/seed/schemas"
)

func (r *measureAlarmResolver) ID(ctx context.Context, obj *schemas.MeasureAlarm) (string, error) {
	return obj.ID.String(), nil
}

func (r *measureAlarmResolver) High(ctx context.Context, obj *schemas.MeasureAlarm) (*schemas.Measure, error) {
	var err error
	var cvrtUUID uuid.UUID
	if cvrtUUID, err = uuid.FromString(*obj.HighID); err != nil {
		return nil, err
	}
	var measure = schemas.Measure{
		Base: schemas.Base{
			ID: cvrtUUID,
		},
	}
	if err = r.DB.First(&measure).Error; err != nil {
		return nil, err
	}
	return &measure, err
}

func (r *measureAlarmResolver) Low(ctx context.Context, obj *schemas.MeasureAlarm) (*schemas.Measure, error) {
	var err error
	var cvrtUUID uuid.UUID
	if cvrtUUID, err = uuid.FromString(*obj.LowID); err != nil {
		return nil, err
	}
	var measure = schemas.Measure{
		Base: schemas.Base{
			ID: cvrtUUID,
		},
	}
	if err = r.DB.First(&measure).Error; err != nil {
		return nil, err
	}
	return &measure, err
}

// MeasureAlarm returns generated.MeasureAlarmResolver implementation.
func (r *Resolver) MeasureAlarm() generated.MeasureAlarmResolver { return &measureAlarmResolver{r} }

type measureAlarmResolver struct{ *Resolver }
