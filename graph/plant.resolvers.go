package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"api/graph/generated"
	"context"

	"gitlab.com/growy1/seed/schemas"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func (r *plantResolver) ID(ctx context.Context, obj *schemas.Plant) (string, error) {
	return obj.ID.String(), nil
}

func (r *plantResolver) Strain(ctx context.Context, obj *schemas.Plant) (*schemas.Strain, error) {
	var err error
	var res = schemas.Strain{
		Model: gorm.Model{
			ID: obj.StrainID,
		},
	}
	if err = r.DB.Preload(clause.Associations).Find(&res).Error; err != nil {
		return nil, err
	}
	return &res, nil
}

func (r *plantResolver) Harvest(ctx context.Context, obj *schemas.Plant) (*schemas.HarvestStock, error) {
	var err error
	var res = schemas.HarvestStock{
		PlantID: obj.ID.String(),
	}
	if err = r.DB.First(&res).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, nil
		}
		return nil, err
	}
	return &res, err
}

// Plant returns generated.PlantResolver implementation.
func (r *Resolver) Plant() generated.PlantResolver { return &plantResolver{r} }

type plantResolver struct{ *Resolver }
