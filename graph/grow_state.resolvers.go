package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"api/graph/generated"
	"context"

	"gitlab.com/growy1/seed/schemas"
)

func (r *growStateResolver) ID(ctx context.Context, obj *schemas.GrowState) (string, error) {
	return obj.ID.String(), nil
}

// GrowState returns generated.GrowStateResolver implementation.
func (r *Resolver) GrowState() generated.GrowStateResolver { return &growStateResolver{r} }

type growStateResolver struct{ *Resolver }
