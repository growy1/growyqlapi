package auth

import (
	"api/graph/secrets"
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/growy1/seed/schemas"

	"gorm.io/gorm"

	"github.com/dgrijalva/jwt-go"
	"github.com/gofrs/uuid"
)

// GinContextKey iiis
type GinContextKey string

// ContextUser is the struct for user in context
type ContextUser struct {
	ID   string
	Name string
	Role schemas.Role
}

func validateAndGetUserID(cookieval string) (userID string, err error) {
	var token *jwt.Token
	var claims jwt.StandardClaims
	HMACSigningSecret := secrets.MustGetSecret("SIGNING_SECRET")

	if token, err = jwt.ParseWithClaims(cookieval, &claims, func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(HMACSigningSecret), nil
	}); err != nil {
		return "", err
	}
	if !token.Valid {
		return "", errors.New("invalid token")
	}
	if err = claims.Valid(); err != nil {
		log.Printf("[ERROR] Error validating token : %s\n", err.Error())
		return "", errors.New("invalid claim")
	}
	return claims.Audience, nil
}

// GinContextToContextMiddleware set context.context
func GinContextToContextMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx := context.WithValue(c.Request.Context(), GinContextKey("gin"), c)
		c.Request = c.Request.WithContext(ctx)
		c.Next()
	}
}

// GinContextFromContext retrieves it
func GinContextFromContext(ctx context.Context) (*gin.Context, error) {
	ginContext := ctx.Value(GinContextKey("gin"))
	if ginContext == nil {
		err := fmt.Errorf("could not retrieve gin.Context")
		return nil, err
	}

	gc, ok := ginContext.(*gin.Context)
	if !ok {
		err := fmt.Errorf("gin.Context has wrong type")
		return nil, err
	}
	return gc, nil
}

// Middleware decodes the share session cookie and packs the session into context
func Middleware(db *gorm.DB) gin.HandlerFunc {
	return func(c *gin.Context) {
		basicAccess := secrets.MustGetSecret("BASIC_ACCESS")
		cook, _ := c.Cookie("auth-cookie")
		if cook == "" {
			cook = c.GetHeader("Authorization")
		}
		if cook == "" {
			c.AbortWithStatusJSON(http.StatusForbidden, map[string]string{"err": "missing authent"})
			return
		} else if cook == "Basic "+basicAccess ||
			cook == basicAccess {
			c.Next()
			return
		}
		// if cook == "" {
		// 	c.Next()
		// 	return
		// }
		userID, err := validateAndGetUserID(cook)
		if err != nil {
			log.Printf("[ERROR]: Failed to validate authorization : %s\n", err.Error())
			c.AbortWithStatusJSON(http.StatusForbidden, map[string]string{"err": "missing authent"})
			return
		}
		var cvrtUUID uuid.UUID
		if cvrtUUID, err = uuid.FromString(userID); err != nil {
			log.Printf("[ERROR] Failed to convert id %s to uuid\n", userID)
			// http.Error(w, "Invalid cookie", http.StatusForbidden)
			c.AbortWithStatusJSON(http.StatusForbidden, map[string]string{"err": "unknow user"})
			return
		}
		var checkUser = schemas.User{
			Base: schemas.Base{
				ID: cvrtUUID,
			},
		}
		if err = db.Select("role", "name").First(&checkUser).Error; err != nil {
			log.Printf("[ERROR] Failed to get user id %s\n", userID)
			// http.Error(w, "account deleted", http.StatusForbidden)
			c.AbortWithStatusJSON(http.StatusForbidden, map[string]string{"err": "user not found"})
			return
		}
		ctxUser := ContextUser{
			ID:   userID,
			Name: checkUser.Name,
			Role: checkUser.Role,
		}
		c.Set("user", ctxUser)
		c.Next()
		return
	}
}

// ForContext finds the user from the context. REQUIRES Middleware to have run.
func ForContext(ctx *gin.Context) *ContextUser {
	var ok bool
	var raw ContextUser
	if raw, ok = ctx.Value("user").(ContextUser); !ok {
		return nil
	}
	return &raw
}
