package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"api/graph/generated"
	"context"

	"github.com/gofrs/uuid"
	"gitlab.com/growy1/seed/schemas"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func (r *deviceStockResolver) ID(ctx context.Context, obj *schemas.DeviceStock) (string, error) {
	return obj.ID.String(), nil
}

func (r *deviceStockResolver) Device(ctx context.Context, obj *schemas.DeviceStock) (*schemas.Device, error) {
	findDevice := schemas.Device{
		Model: gorm.Model{
			ID: obj.DeviceID,
		},
	}
	if err := r.DB.Find(&findDevice).Error; err != nil {
		return nil, err
	}
	return &findDevice, nil
}

func (r *harvestStockResolver) ID(ctx context.Context, obj *schemas.HarvestStock) (string, error) {
	return obj.ID.String(), nil
}

func (r *harvestStockResolver) Plant(ctx context.Context, obj *schemas.HarvestStock) (*schemas.Plant, error) {
	var err error
	plantUUID, err := uuid.FromString(obj.PlantID)
	if err != nil {
		return nil, err
	}
	findPlant := schemas.Plant{
		Base: schemas.Base{
			ID: plantUUID,
		},
	}
	if err := r.DB.Find(&findPlant).Error; err != nil {
		return nil, err
	}
	return &findPlant, nil
}

func (r *inventoryResolver) ID(ctx context.Context, obj *schemas.Inventory) (string, error) {
	return obj.ID.String(), nil
}

func (r *seedStockResolver) ID(ctx context.Context, obj *schemas.SeedStock) (string, error) {
	return obj.ID.String(), nil
}

func (r *seedStockResolver) Strain(ctx context.Context, obj *schemas.SeedStock) (*schemas.Strain, error) {
	findStrain := schemas.Strain{
		Model: gorm.Model{
			ID: obj.StrainID,
		},
	}
	if err := r.DB.Preload(clause.Associations).Find(&findStrain).Error; err != nil {
		return nil, err
	}
	return &findStrain, nil
}

// DeviceStock returns generated.DeviceStockResolver implementation.
func (r *Resolver) DeviceStock() generated.DeviceStockResolver { return &deviceStockResolver{r} }

// HarvestStock returns generated.HarvestStockResolver implementation.
func (r *Resolver) HarvestStock() generated.HarvestStockResolver { return &harvestStockResolver{r} }

// Inventory returns generated.InventoryResolver implementation.
func (r *Resolver) Inventory() generated.InventoryResolver { return &inventoryResolver{r} }

// SeedStock returns generated.SeedStockResolver implementation.
func (r *Resolver) SeedStock() generated.SeedStockResolver { return &seedStockResolver{r} }

type deviceStockResolver struct{ *Resolver }
type harvestStockResolver struct{ *Resolver }
type inventoryResolver struct{ *Resolver }
type seedStockResolver struct{ *Resolver }
