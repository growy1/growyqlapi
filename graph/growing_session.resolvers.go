package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"api/graph/generated"
	"context"

	"gitlab.com/growy1/seed/schemas"
)

func (r *growingSessionResolver) ID(ctx context.Context, obj *schemas.GrowingSession) (string, error) {
	return obj.ID.String(), nil
}

func (r *growingSessionResolver) Lastpicture(ctx context.Context, obj *schemas.GrowingSession) (*schemas.Picture, error) {
	var err error
	var elm schemas.Picture
	// if err = r.DB.
	// Last(&elm)
	if err = r.DB.Joins("JOIN growingsession_pictures ON picture_id = pictures.id WHERE growing_session_id = ?", obj.ID.String()).
		Last(&elm).Error; err != nil {
		return nil, err
	}
	return &elm, err
}

func (r *growingSessionResolver) Measures(ctx context.Context, obj *schemas.GrowingSession) ([]schemas.Measure, error) {
	var err error
	var elems []schemas.Measure
	if err = r.DB.
		Joins("JOIN growingsession_measures ON measure_id = measures.id WHERE growing_session_id = ?", obj.ID.String()).
		Order("date DESC").
		Find(&elems).Error; err != nil {
		return nil, err
	}
	return elems, nil
}

func (r *growingSessionResolver) Lastmeasure(ctx context.Context, obj *schemas.GrowingSession) (*schemas.Measure, error) {
	var err error
	var elem schemas.Measure
	if err = r.DB.
		Joins("JOIN growingsession_measures ON measure_id = measures.id WHERE growing_session_id = ?", obj.ID.String()).
		Order("date DESC").
		Last(&elem).Error; err != nil {
		return nil, err
	}
	return &elem, nil
}

// GrowingSession returns generated.GrowingSessionResolver implementation.
func (r *Resolver) GrowingSession() generated.GrowingSessionResolver {
	return &growingSessionResolver{r}
}

type growingSessionResolver struct{ *Resolver }
