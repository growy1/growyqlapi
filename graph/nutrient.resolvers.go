package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"api/graph/generated"
	"context"

	"gitlab.com/growy1/seed/schemas"
)

func (r *nutrientResolver) ID(ctx context.Context, obj *schemas.Nutrient) (int, error) {
	return int(obj.ID), nil
}

func (r *nutrientResolver) Npk(ctx context.Context, obj *schemas.Nutrient) ([]int, error) {
	res := obj.NPK()
	if res != nil {
		cvrt := *res
		var fmt []int
		for el := range cvrt {
			fmt = append(fmt, int(cvrt[el]))
		}
		return fmt, nil
	}
	return nil, nil
}

func (r *nutrientSetResolver) ID(ctx context.Context, obj *schemas.NutrientSet) (int, error) {
	return int(obj.ID), nil
}

func (r *plantStateResolver) ID(ctx context.Context, obj *schemas.PlantState) (int, error) {
	return int(obj.ID), nil
}

// Nutrient returns generated.NutrientResolver implementation.
func (r *Resolver) Nutrient() generated.NutrientResolver { return &nutrientResolver{r} }

// NutrientSet returns generated.NutrientSetResolver implementation.
func (r *Resolver) NutrientSet() generated.NutrientSetResolver { return &nutrientSetResolver{r} }

// PlantState returns generated.PlantStateResolver implementation.
func (r *Resolver) PlantState() generated.PlantStateResolver { return &plantStateResolver{r} }

type nutrientResolver struct{ *Resolver }
type nutrientSetResolver struct{ *Resolver }
type plantStateResolver struct{ *Resolver }
