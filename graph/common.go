package graph

import (
	"os"

	"github.com/gofrs/uuid"
)

// APIUser is the user for backend http service
var APIUser = os.Getenv("API_USER")

// APIKey is the api key for backend http service
var APIKey = os.Getenv("API_KEY")

// Stage is the stage
var Stage = func() string {
	envar := os.Getenv("STAGE")
	if envar == "" {
		return "DEV"
	}
	return envar
}()

// Verbose is log verbosity level
var Verbose = func() bool {
	return os.Getenv("VERBOSE") != ""
}()

// PhotoRequest is a struct to get picture saved by paparazzi service
type PhotoRequest struct {
	GrowingSessionID uuid.UUID  `json:"growingSessionId"`
	PlantID          *uuid.UUID `json:"plantId"`
}

// DropRequest is a growing session drop photo request
type DropRequest struct {
	FilePath string `json:"filePath"`
}

// LoginRequest is a login request
type LoginRequest struct {
	Name     string  `json:"name"`
	Password *string `json:"password"`
	OTP      *int    `json:"otp"`
}
