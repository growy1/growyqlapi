package secrets

import (
	"io/ioutil"
	"log"
	"os"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
)

// Context is the running context
var Context = os.Getenv("CONTEXT")

func getKubeSecret(secretName string) (secretVal string, err error) {
	var secretDir = os.Getenv("SECRETS_PATH")
	var secretBytes []byte
	if strings.Contains(secretName, "_") {
		secretName = strings.ReplaceAll(secretName, "_", "-")
	}
	secretName = strings.ToLower(secretName)
	if secretBytes, err = ioutil.ReadFile(secretDir + secretName); err != nil {
		return "", err
	}
	secretString := strings.TrimSuffix(string(secretBytes), "\n")
	return secretString, err
}

// MustGetSecret from secret storage or env var (panic if failed)
func MustGetSecret(secretName string) (secret string) {
	var err error
	defVal := os.Getenv(secretName)
	if Context == "KUBE" {
		if defVal, err = getKubeSecret(secretName); err != nil {
			log.Fatal(err)
		}
	}
	if defVal == "" {
		log.Fatalf("missing secret %s", secretName)
	}
	return defVal
}

// GetTokenString return a jwt as string after a successfull authentication
func GetTokenString(userID string) (token string, err error) {
	// HMACSigningSecret is ...
	var HMACSigningSecret = MustGetSecret("SIGNING_SECRET")
	tokenObj := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.StandardClaims{
		Audience:  userID,
		Issuer:    "api.aerogrow.org",
		ExpiresAt: time.Now().Add(3 * time.Hour).Unix(),
	})
	// Sign and get the complete encoded token as a string using the secret
	return tokenObj.SignedString([]byte(HMACSigningSecret))
}
