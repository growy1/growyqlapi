package handlers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/growy1/seed/schemas"
	"gorm.io/gorm"
)

// GetAlarmsHandler is a rest handler to get all alarm settings
func GetAlarmsHandler(db *gorm.DB) gin.HandlerFunc {
	return func(c *gin.Context) {
		var err error
		var res []schemas.MeasureAlarm
		if err = db.
			Find(&res).Error; err != nil {
			if err == gorm.ErrRecordNotFound {
				c.AbortWithStatusJSON(http.StatusNotFound, map[string]string{"error": "no such alarms"})
				return
			}
			c.AbortWithStatusJSON(http.StatusInternalServerError, map[string]string{"error": "db error"})
			return
		}
		// Load alarm measure values
		for el := range res {
			if res[el].HighID != nil {
				var highmeasure schemas.Measure
				if err = db.
					Where("id = ?", *res[el].HighID).
					First(&highmeasure).
					Error; err != nil {
					c.AbortWithStatusJSON(http.StatusInternalServerError, map[string]string{"error": "db error"})
					return
				}
				res[el].High = &highmeasure
			}
			if res[el].LowID != nil {
				var lowmeasure schemas.Measure
				if err = db.
					Where("id = ?", *res[el].LowID).
					First(&lowmeasure).
					Error; err != nil {
					c.AbortWithStatusJSON(http.StatusInternalServerError, map[string]string{"error": "db error"})
					return
				}
				res[el].Low = &lowmeasure
			}
		}
		c.AbortWithStatusJSON(http.StatusOK, res)
		return
	}
}
