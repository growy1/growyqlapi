package handlers

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/growy1/seed/schemas"
	"gorm.io/gorm"
)

// GetGrowingSessionPictures return picture array for a growing session
func GetGrowingSessionPictures(db *gorm.DB) gin.HandlerFunc {
	return func(c *gin.Context) {
		var err error
		var elems []schemas.Picture
		var growingSessionID = c.Param("growingsessionid")
		if err = db.
			Joins("JOIN growingsession_pictures ON picture_id = pictures.id WHERE growing_session_id = ?", growingSessionID).
			Order("taken_at DESC").
			Find(&elems).Error; err != nil {
			fmt.Printf("[ERROR] failed to unmarshall payload : %s\n", err.Error())
			c.AbortWithStatusJSON(http.StatusBadRequest, map[string]string{"error": "bad request"})
			return
		}
		c.AbortWithStatusJSON(http.StatusOK, elems)
		return
	}
}
