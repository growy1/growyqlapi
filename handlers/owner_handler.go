package handlers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/growy1/seed/schemas"
	"gorm.io/gorm"
)

// GetOwnerHandler is a rest handler to get user notification param for a growing session id
func GetOwnerHandler(db *gorm.DB) gin.HandlerFunc {
	return func(c *gin.Context) {
		var err error
		var res schemas.User
		var growingSessionID = c.Param("growingsessionid")
		if err = db.
			Joins("JOIN user_growingsessions ON user_id = users.id").
			Where("growing_session_id = ?", growingSessionID).
			First(&res).
			Error; err != nil {
			if err == gorm.ErrRecordNotFound {
				c.AbortWithStatusJSON(http.StatusNotFound, map[string]string{"error": "no such growing session or user not found"})
				return
			}
			c.AbortWithStatusJSON(http.StatusInternalServerError, map[string]string{"error": "db error"})
			return
		}
		c.AbortWithStatusJSON(http.StatusOK, res)
		return
	}
}
