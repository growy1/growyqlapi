package handlers

import (
	"errors"
	"fmt"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/gofrs/uuid"
	"gitlab.com/growy1/seed/schemas"
	"gorm.io/gorm"
)

// ErrNoMeasureFound is returned when no measure found
var ErrNoMeasureFound = errors.New("no measures saved for this growing session or invalid data type")

// GetLastMeasureHandler is a rest handler to get latest recorded sensor data
func GetLastMeasureHandler(db *gorm.DB) gin.HandlerFunc {
	return func(c *gin.Context) {
		var err error
		var result schemas.Measure
		var growingSessionID = c.Param("growingsessionid")
		var measureTypeString = c.Param("type")

		measureTypeString = strings.ToUpper(measureTypeString)
		if _, err = uuid.FromString(growingSessionID); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, map[string]string{"error": "bad request"})
			return
		}
		if err = db.
			Joins("JOIN growingsession_measures ON measure_id = measures.id").
			Where("growing_session_id = ? AND measures.type = ?", growingSessionID, measureTypeString).
			Order("date DESC").
			First(&result).Error; err != nil {
			if err == gorm.ErrRecordNotFound {
				c.AbortWithStatusJSON(http.StatusNotFound, map[string]string{"error": ErrNoMeasureFound.Error()})
				return
			}
			c.AbortWithStatusJSON(http.StatusInternalServerError, map[string]string{"error": "db error"})
			return
		}
		c.AbortWithStatusJSON(http.StatusOK, result)
		return
	}
}

// AddMeasureHandler is a rest handler to push sensor data
func AddMeasureHandler(db *gorm.DB) gin.HandlerFunc {
	return func(c *gin.Context) {
		var err error
		var input []schemas.Measure
		var sessionUUID uuid.UUID
		var growingSessionID = c.Param("growingsessionid")

		if sessionUUID, err = uuid.FromString(growingSessionID); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, map[string]string{"error": "bad request"})
			return
		}
		if err = c.BindJSON(&input); err != nil {
			fmt.Printf("[ERROR] failed to unmarshall payload : %s\n", err.Error())
			c.AbortWithStatusJSON(http.StatusBadRequest, map[string]string{"error": "bad request"})
			return
		}
		updatedGrowingSession := schemas.GrowingSession{
			Base: schemas.Base{
				ID: sessionUUID,
			},
			Measures: input,
		}
		if err = db.Updates(&updatedGrowingSession).Error; err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, map[string]string{"error": "db error"})
			return
		}
		c.AbortWithStatus(http.StatusOK)
		return
	}
}
