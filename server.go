package main

import (
	"api/graph"
	"api/graph/auth"
	"api/graph/generated"
	"api/graph/secrets"
	"api/handlers"
	"log"
	"os"

	"gitlab.com/growy1/seed/schemas"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/gin-gonic/gin"
	"gitlab.com/growy1/seed/migrator"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

const defaultPort = "8080"

// Defining the Graphql handler
func graphqlHandler(db *gorm.DB) gin.HandlerFunc {
	// NewExecutableSchema and Config are in the generated.go file
	// Resolver is in the resolver.go file
	c := generated.Config{
		Resolvers: &graph.Resolver{
			DB: db,
		},
	}
	c.Directives.HasRole = graph.HasRole
	srv := handler.NewDefaultServer(generated.NewExecutableSchema(c))
	return func(c *gin.Context) {
		srv.ServeHTTP(c.Writer, c.Request)
	}
}

// Defining the Playground handler
func playgroundHandler() gin.HandlerFunc {
	h := playground.Handler("GraphQL", "/query")

	return func(c *gin.Context) {
		h.ServeHTTP(c.Writer, c.Request)
	}
}

func main() {
	godEmail := secrets.MustGetSecret("GOD_EMAIL")
	godPassword := secrets.MustGetSecret("GOD_PASSWORD")
	godUser := schemas.User{
		Name:     secrets.MustGetSecret("GOD_USER"),
		Email:    &godEmail,
		Role:     schemas.God,
		Password: &godPassword,
	}
	dbPath := os.Getenv("DB_PATH")
	if dbPath == "" {
		dbPath = "storage/growy.db"
	}
	// dbURL := secrets.MustGetSecret("DB_URL")
	port := os.Getenv("PORT")
	if port == "" {
		port = defaultPort
	}
	// db, err := gorm.Open(postgres.Open(dbURL), &gorm.Config{})
	db, err := gorm.Open(sqlite.Open(dbPath), &gorm.Config{})
	if err != nil {
		log.Fatal(err)
	}
	if err = migrator.Migrate(db); err != nil {
		log.Fatal(err)
	}
	if err = db.Clauses(clause.OnConflict{
		Columns:   []clause.Column{{Name: "name"}},                     // key colume
		DoUpdates: clause.AssignmentColumns([]string{"password_hash"}), // column needed to be updated
	}).Create(&godUser).Error; err != nil {
		log.Fatal(err)
	}
	r := gin.Default()
	r.GET("/", playgroundHandler())
	r.Use(auth.GinContextToContextMiddleware())
	r.POST("/query", auth.Middleware(db), graphqlHandler(db))
	checkAPIAuth := r.Group("/api/v1", auth.Middleware(db))
	checkAPIAuth.GET("/alarms", handlers.GetAlarmsHandler(db))
	checkAPIAuth.GET("/owner/:growingsessionid", handlers.GetOwnerHandler(db))
	checkAPIAuth.POST("/measures/:growingsessionid", handlers.AddMeasureHandler(db))
	checkAPIAuth.GET("/measures/:growingsessionid/:type", handlers.GetLastMeasureHandler(db))
	checkAPIAuth.GET("/pictures/:growingsessionid", handlers.GetGrowingSessionPictures(db))

	log.Fatal(r.Run(":" + port))
}
